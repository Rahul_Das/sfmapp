'use strict'

export class Box {
  top: number
  right: number
  bottom: number
  left: number

  constructor(top: number, right: number, bottom: number, left: number) {
    this.top = top
    this.right = right
    this.bottom = bottom
    this.left = left
  }
}

export class Dimension {
  width: number
  height: number

  constructor(width: number, height: number) {
    this.width = width
    this.height = height
  }

  substract(box: Box): Dimension {
    return new Dimension(this.width - box.left - box.right, this.height - box.top - box.bottom)
  }
}
