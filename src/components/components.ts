'use strict'

import {IDisposable, Disposable, dispose} from '../base/disposable'
import {Dimension} from './interfaces'

const TITLE_HEIGHT = 35

export interface IComponent extends IDisposable {
  getId(): string
  //shutdown(): void
  create(parent: HTMLDivElement, clsNames: string[]): void
  layout(dimension: Dimension): Dimension[]
  dispose(): void
}

export interface IPartOptions {
  hasTitle?: boolean
}

export abstract class Component extends Disposable implements IComponent {
  private _toUnbind: IDisposable[]
  private id: string
  private parent: HTMLDivElement
  private titleArea: HTMLDivElement
  protected container: HTMLDivElement

  constructor(id: string, private options: IPartOptions) {
    super()

    this._toUnbind = []
    this.id = id
    this.container = {} as HTMLDivElement
    this.titleArea = {} as HTMLDivElement
    this.parent = {} as HTMLDivElement
  }

  create(parent: HTMLDivElement): void {
    this.parent = parent

    this.container = this.createContentArea(parent)
  }

  abstract createContentArea(parent: HTMLDivElement): HTMLDivElement

  protected getContentArea(): HTMLDivElement {
    return this.container
  }

  layout(dimension: Dimension): Dimension[] {
    const {width, height} = dimension
    const sizes: Dimension[] = []
    let titleSize: Dimension
    if (this.options && this.options.hasTitle) {
      titleSize = new Dimension(width, Math.min(height, TITLE_HEIGHT))
    } else {
      titleSize = new Dimension(0, 0)
    }
    const contentSize = new Dimension(width, height - titleSize.height)
    sizes.push(titleSize)
    sizes.push(contentSize)
    // Content
    if (this.container) {
      this.container.style.width = contentSize.width.toString()
      this.container.style.height = contentSize.height.toString()
    }
    return sizes
  }

  protected get toUnbind(): IDisposable[] {
    return this._toUnbind
  }

  getId(): string {
    return this.id
  }

  dispose(): void {
    this._toUnbind = dispose(this._toUnbind)
    super.dispose()
  }
}
