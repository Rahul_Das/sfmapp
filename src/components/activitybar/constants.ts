import {IStatusbarItem} from './Item'

export const StatusBarItems: IStatusbarItem[] = [
  {text: 'workspace_name', alignment: 'LEFT', tooltip: 'workspace_name', color: 'white'},
  {text: 'notification', alignment: 'RIGHT', tooltip: 'workspace_name', color: 'white'},
  {text: 'notification', alignment: 'LEFT', tooltip: 'workspace_name', color: 'white'},
  {text: 'notification', alignment: 'LEFT', tooltip: 'workspace_name', color: 'white'},
  {text: 'notification', alignment: 'RIGHT', tooltip: 'workspace_name', color: 'white'},
]
