'use strict'

import {IDisposable, dispose} from '../../base/disposable'
import {Component} from '../components'
import {StatusBarItem, IStatusbarItem} from './Item'
import {StatusBarItems} from './constants'
import './activitybar.scss'

export class Activitybar extends Component {
  private toDispose: IDisposable[]
  private container: HTMLDivElement

  constructor(id: string) {
    super(id, {hasTitle: false})
    this.toDispose = []
    this.container = {} as HTMLDivElement
  }

  createContentArea(parent: HTMLDivElement): HTMLDivElement {
    const element = document.createElement('div')
    element.classList.add('status-bar')
    this.toDispose.push(...StatusBarItems.map((item: IStatusbarItem) => this.addEntry(item)))
    parent?.appendChild(element)
    return element
  }

  getComputedStyle(): CSSStyleDeclaration {
    return window.getComputedStyle(this.container, null)
  }

  addEntry(entry: IStatusbarItem): IDisposable {
    const statusBarItem = new StatusBarItem(entry)
    const toDispose = statusBarItem.render()
    statusBarItem.additemtoParent(this.container)
    return {
      dispose: () => {
        if (toDispose) {
          toDispose.dispose()
        }
        statusBarItem.destroy()
      },
    }
  }

  dispose(): void {
    this.toDispose = dispose(this.toDispose)
    super.dispose()
  }
}
