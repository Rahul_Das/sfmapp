/* eslint-disable @typescript-eslint/no-non-null-assertion */
'use strict'

import {IDisposable} from '../../base/disposable'
import {EventEmitter} from '../../base/event/eventemiter'
import {classids} from '../../layoutConstant'

import {creteIcons} from '../../ulility/iconmaker'
import {IIconmakerOptions} from '../../ulility/iconMakerOpt'

export interface ISidebarItem {
  id: string
  icon: IIconmakerOptions
  alignment: 'UP' | 'DOWN'
  tooltip?: string
  color?: string
  command?: string
  eventName: string[]
}

export class SideBarItem extends EventEmitter {
  private _element: HTMLDivElement | null
  private _entry: ISidebarItem

  constructor(_entry: ISidebarItem) {
    super(_entry.eventName)
    this._entry = _entry
    this._element = document.createElement('div')
    this._element.classList.add(classids.sidebar.item)
    this._element.setAttribute('id', _entry.id)
  }

  render(): IDisposable {
    const textContainer = document.createElement('a') as HTMLAnchorElement
    textContainer.addEventListener('click', () => this.executeCommand(this._entry.command!))
    const svgIcon = creteIcons(this._entry.icon)
    textContainer.appendChild(svgIcon)
    if (this._entry.tooltip) {
      textContainer.setAttribute('title', this._entry.tooltip)
    }
    this._element?.appendChild(textContainer)
    return {
      dispose: () => {
        textContainer.removeEventListener('click', () => this.executeCommand(this._entry.command!))
      },
    }
  }

  destroy() {
    if (this._element) {
      if (this._element.parentNode) {
        this._element.parentNode.removeChild(this._element)
      }
    }
    this.dispose()
    this._element = null
  }

  private executeCommand(command: string) {
    this.emit('click', {id: this._entry.id})
    //console.log(`Action ${command}`)
  }

  getElement(): HTMLDivElement | null {
    return this._element
  }

  getId(): string {
    return this._entry.id
  }

  toggleActive(): void {
    this._element?.classList.toggle('active')
  }

  removeActive(): void {
    this._element?.classList.remove('active')
  }
}
