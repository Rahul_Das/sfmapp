'use strict'

import {IDisposable, dispose} from '../../base/disposable'
import {Component} from '../components'
import {SideBarItem, ISidebarItem} from './Item'
import {SideBarUpItems, SideBarDownItems} from './constants'
import {classids} from '../../layoutConstant'
import './sidebar.scss'

export interface ISideBar {
  create(element: HTMLDivElement): void
  getComputedStyle(): CSSStyleDeclaration
  addEntry(entry: ISidebarItem): IDisposable
  dispose(): void
}

export class SideBar extends Component {
  private _toDispose: IDisposable[]
  private _upContainer!: HTMLDivElement
  private _downContainer!: HTMLDivElement
  private _sideBarItemList: SideBarItem[]

  constructor(id: string) {
    super(id, {hasTitle: false})
    this._toDispose = []
    this._sideBarItemList = []
  }

  private _onClickHandler: (data: {id: string}) => void = (data) => {
    this._sideBarItemList.forEach((item: SideBarItem) => {
      if (item.getId() === data.id) {
        item.toggleActive()
      } else {
        item.removeActive()
      }
    })
  }

  createContentArea(parent: HTMLDivElement): HTMLDivElement {
    const container = document.createElement('div')
    container.classList.add(classids.sidebar.content)
    this._upContainer = document.createElement('div')
    this._upContainer.classList.add('up')
    this._downContainer = document.createElement('div')
    this._downContainer.classList.add('down')
    this._toDispose.push(
      ...SideBarUpItems.map((item: ISidebarItem) => {
        const sideBarItem = new SideBarItem(item)
        const toDispose = sideBarItem.render()
        const element = sideBarItem.getElement()
        if (element) {
          this._upContainer.appendChild(element)
        }
        this._sideBarItemList.push(sideBarItem)
        sideBarItem.addListener('click', this._onClickHandler)
        return {
          dispose: () => {
            if (toDispose) {
              toDispose.dispose()
            }
            sideBarItem.destroy()
          },
        }
      })
    )
    this._toDispose.push(
      ...SideBarDownItems.map((item: ISidebarItem) => {
        const sideBarItem = new SideBarItem(item)
        const toDispose = sideBarItem.render()
        const element = sideBarItem.getElement()
        if (element) {
          this._downContainer.appendChild(element)
        }
        this._sideBarItemList.push(sideBarItem)
        sideBarItem.addListener('click', this._onClickHandler)
        return {
          dispose: () => {
            if (toDispose) {
              toDispose.dispose()
            }
            sideBarItem.destroy()
          },
        }
      })
    )
    container.appendChild(this._upContainer)
    container.appendChild(this._downContainer)
    parent?.appendChild(container)
    return container
  }

  getComputedStyle(): CSSStyleDeclaration {
    return window.getComputedStyle(this.container, null)
  }

  addEntry(item: ISidebarItem): IDisposable {
    const sideBarItem = new SideBarItem(item)
    const toDispose = sideBarItem.render()
    const element = sideBarItem.getElement()
    if (element) {
      if (item.alignment == 'UP') {
        this._upContainer.appendChild(element)
      } else {
        this._downContainer.appendChild(element)
      }
    }
    this._sideBarItemList.push(sideBarItem)
    sideBarItem.addListener('click', this._onClickHandler)
    return {
      dispose: () => {
        if (toDispose) {
          toDispose.dispose()
        }
        sideBarItem.destroy()
      },
    }
  }

  dispose(): void {
    this._toDispose = dispose(this._toDispose)
    super.dispose()
  }
}
