import {ISidebarItem} from './Item'

export const SideBarUpItems: ISidebarItem[] = [
  {
    id: 'id1',
    icon: {name: 'addProject', className: 'sideber-icon'},
    alignment: 'UP',
    tooltip: 'addProject',
    color: 'white',
    eventName: ['addProject', 'click'],
  },
]

export const SideBarDownItems: ISidebarItem[] = [
  {
    id: 'id2',
    icon: {name: 'settings', className: 'sideber-icon'},
    alignment: 'DOWN',
    tooltip: 'settings',
    color: 'white',
    eventName: ['openSettings', 'click'],
  },
  {
    id: 'id3',
    icon: {name: 'account', className: 'sideber-icon'},
    alignment: 'DOWN',
    tooltip: 'account',
    color: 'white',
    eventName: ['showHistory', 'click'],
  },
]
