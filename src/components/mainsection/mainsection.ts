'use strict'

import {IDisposable, dispose} from '../../base/disposable'
import {Component} from '../components'
import {classids} from '../../layoutConstant'
import './mainSection.scss'

export class MainSection extends Component {
  private toDispose: IDisposable[]


  constructor(id: string) {
    super(id, {hasTitle: false})
    this.toDispose = []
    this.container = {} as HTMLDivElement
  }

  createContentArea(parent: HTMLDivElement): HTMLDivElement {
    const element = document.createElement('div')
    element.classList.add(classids.mainsection.content)
    //this.toDispose.push(...StatusBarItems.map((item: IStatusbarItem) => this.addEntry(item)))
    parent?.appendChild(element)
    return element
  }

  getComputedStyle(): CSSStyleDeclaration {
    return window.getComputedStyle(this.container, null)
  }

  /* addEntry(entry: IStatusbarItem): IDisposable {
  } */

  dispose(): void {
    this.toDispose = dispose(this.toDispose)
    super.dispose()
  }
}
