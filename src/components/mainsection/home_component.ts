'use strict'

import {IDisposable} from '../../base/disposable'

export interface IStatusbarItem {
  text: string
  alignment: 'LEFT' | 'RIGHT'
  tooltip?: string
  color?: string
  command?: string
}

export class HomeComponent {
  private element: HTMLDivElement | null

  constructor(private entry: IStatusbarItem) {
    this.element = document.createElement('div')
    this.element.classList.add('home-component')
  }

  render(): IDisposable {
    let textContainer: HTMLElement
    if (this.entry.command) {
      textContainer = document.createElement('a') as HTMLAnchorElement
      textContainer.addEventListener('click', () => this.executeCommand(this.entry.command!))
    } else {
      textContainer = document.createElement('span') as HTMLSpanElement
    }
    if (this.entry.tooltip) {
      textContainer.setAttribute('title', this.entry.tooltip)
    }
    if (this.entry.color) {
      textContainer.style.color = this.entry.color
    }
    this.element?.appendChild(textContainer)
    return {
      dispose: () => {
        if (this.entry.command) {
          textContainer.removeEventListener('click', () => this.executeCommand(this.entry.command!))
        }
      },
    }
  }

  additemtoParent(parent: HTMLElement) {
    if (this.element) {
      parent.appendChild(this.element)
    }
  }

  destroy() {
    if (this.element) {
      if (this.element.parentNode) {
        this.element.parentNode.removeChild(this.element)
      }
    }
    this.element = null
  }

  private executeCommand(id: string) {
    console.log(`Action ${id}`)
  }
}
