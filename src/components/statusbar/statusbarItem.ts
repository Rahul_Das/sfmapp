/* eslint-disable @typescript-eslint/no-non-null-assertion */
'use strict'

import { IDisposable } from '../../base/disposable'
import { classids } from '../../layoutConstant'
import { creteIcons } from '../../ulility/iconmaker'
import { IIconmakerOptions } from '../../ulility/iconMakerOpt'

export interface IStatusbarItem {
  id: string
  text?: string
  icon?: IIconmakerOptions
  alignment: 'LEFT' | 'RIGHT'
  tooltip?: string
  color?: string
  command?: string
}

export class StatusBarItem {
  private _element: HTMLDivElement | null
  private _entry: IStatusbarItem

  constructor(entry: IStatusbarItem) {
    this._entry = entry
    this._element = document.createElement('div')
    this._element.classList.add(classids.statusbar.item)
    this._element.setAttribute('id', entry.id)
  }

  render(): IDisposable {
    let textContainer: HTMLElement
    if (this._entry.icon) {
      textContainer = document.createElement('a') as HTMLAnchorElement
      textContainer.addEventListener('click', () => this.executeCommand(this._entry.command!))
      const svgIcon = creteIcons(this._entry.icon)
      textContainer.appendChild(svgIcon)
    } else if (this._entry.text) {
      textContainer = document.createElement('span') as HTMLSpanElement
      textContainer.textContent = this._entry.text
    } else {
      throw new Error('Can not render Status BAr Item , Invalid citem config')
    }
    if (this._entry.tooltip) {
      textContainer.setAttribute('title', this._entry.tooltip)
    }
    if (this._entry.color) {
      textContainer.style.color = this._entry.color
    }
    this._element?.appendChild(textContainer)
    return {
      dispose: () => {
        if (this._entry.command) {
          textContainer.removeEventListener('click', () => this.executeCommand(this._entry.command!))
        }
      },
    }
  }

  destroy() {
    if (this._element) {
      if (this._element.parentNode) {
        this._element.parentNode.removeChild(this._element)
      }
    }
    this._element = null
  }

  private executeCommand(id: string) {
    console.log(`Action ${id}`)
  }

  getElement(): HTMLDivElement | null {
    return this._element
  }
}
