'use strict'

import {IDisposable, dispose} from '../../base/disposable'
import {Component} from '../components'
import {StatusBarItem, IStatusbarItem} from './statusbarItem'
import {classids} from '../../layoutConstant'
import {StatusBarRightItems, StatusBarLeftItems} from './constants'
import './statusbar.scss'

export interface IStatusBar {
  create(element: HTMLDivElement): void
  getComputedStyle(): CSSStyleDeclaration
  addEntry(entry: IStatusbarItem): IDisposable
  dispose(): void
}

export class StatusBar extends Component implements IStatusBar {
  private _toDispose: IDisposable[]
  private _leftContainer!: HTMLDivElement
  private _rightContainer!: HTMLDivElement
  private _itemList: StatusBarItem[]

  constructor(id: string) {
    super(id, {hasTitle: false})
    this._toDispose = []
    this._itemList = []
  }

  createContentArea(parent: HTMLDivElement): HTMLDivElement {
    const container = document.createElement('div')
    container.classList.add(classids.statusbar.content)
    this._leftContainer = document.createElement('div')
    this._leftContainer.classList.add('left')
    this._rightContainer = document.createElement('div')
    this._rightContainer.classList.add('right')
    this._toDispose.push(
      ...StatusBarLeftItems.map((item: IStatusbarItem) => {
        const statusBarItem = new StatusBarItem(item)
        const toDispose = statusBarItem.render()
        const element = statusBarItem.getElement()
        if (element) {
          this._leftContainer.appendChild(element)
        }
        this._itemList.push(statusBarItem)
        return {
          dispose: () => {
            if (toDispose) {
              toDispose.dispose()
            }
            statusBarItem.destroy()
          },
        }
      })
    )
    this._toDispose.push(
      ...StatusBarRightItems.map((item: IStatusbarItem) => {
        const statusBarItem = new StatusBarItem(item)
        const toDispose = statusBarItem.render()
        const element = statusBarItem.getElement()
        if (element) {
          this._rightContainer.appendChild(element)
        }
        this._itemList.push(statusBarItem)
        return {
          dispose: () => {
            if (toDispose) {
              toDispose.dispose()
            }
            statusBarItem.destroy()
          },
        }
      })
    )
    container.appendChild(this._leftContainer)
    container.appendChild(this._rightContainer)
    parent?.appendChild(container)
    return container
  }

  getComputedStyle(): CSSStyleDeclaration {
    return window.getComputedStyle(this.container, null)
  }

  addEntry(entry: IStatusbarItem): IDisposable {
    const statusBarItem = new StatusBarItem(entry)
    const toDispose = statusBarItem.render()
    const element = statusBarItem.getElement()
    if (element) {
      if (entry.alignment === 'LEFT') {
        this._leftContainer.appendChild(element)
      } else {
        this._rightContainer.appendChild(element)
      }
    }
    this._itemList.push(statusBarItem)
    return {
      dispose: () => {
        if (toDispose) {
          toDispose.dispose()
        }
        statusBarItem.destroy()
      },
    }
  }

  dispose(): void {
    this._toDispose = dispose(this._toDispose)
    super.dispose()
  }
}
