import {IStatusbarItem} from './statusbarItem'

export const StatusBarLeftItems: IStatusbarItem[] = [
  {id: 'sb1', text: 'WORK Space Name', alignment: 'LEFT', tooltip: 'workspace_name', color: 'white'},
  {id: 'sb2', text: 'notification', alignment: 'LEFT', tooltip: 'workspace_name', color: 'white'},
  {id: 'sb3', text: 'notification', alignment: 'LEFT', tooltip: 'workspace_name', color: 'white'},
]

export const StatusBarRightItems: IStatusbarItem[] = [
  {
    id: 'sb4',
    icon: {
      name: 'notification',
      className: 'statusber-icon',
      height: '22',
      weidth: '22',
    },
    alignment: 'RIGHT',
    tooltip: 'No Notification',
    color: 'white',
  },
]
