'use strict'

import { IDisposable, dispose } from '../../base/disposable'
import { Component } from '../components'
import { ITabBarItem, TabBarItem } from './tabbarItem'
import { HomeItems } from './constants'
import { classids } from '../../layoutConstant'
import './tabbar.scss'

export interface ITabBar {
  create(element: HTMLDivElement): void
  getComputedStyle(): CSSStyleDeclaration
  addEntry(entry: ITabBarItem): IDisposable
  dispose(): void
}

export class TabBar extends Component implements ITabBar {
  private _toDispose: IDisposable[]
  private _tabContainer!: HTMLDivElement
  private _itemList: TabBarItem[]

  constructor(id: string) {
    super(id, { hasTitle: false })
    this._toDispose = []
    this._itemList = []
  }

  createContentArea(parent: HTMLDivElement): HTMLDivElement {
    this._tabContainer = document.createElement('div')
    this._tabContainer.classList.add(classids.tabbar.content)

    const hometabItem = new TabBarItem(HomeItems)
    const toDispose = hometabItem.render()
    const element = hometabItem.getElement()
    if (element) {
      this._tabContainer.appendChild(element)
    }
    this._itemList.push(hometabItem)

    this._toDispose.push(
      {
        dispose: () => {
          if (toDispose) {
            toDispose.dispose()
          }
          hometabItem.destroy()
        }
      }
    )
    parent?.appendChild(this._tabContainer)
    return this._tabContainer
  }

  getComputedStyle(): CSSStyleDeclaration {
    return window.getComputedStyle(this.container, null)
  }

  addEntry(entry: ITabBarItem): IDisposable {
    const tabBarItem = new TabBarItem(entry)
    const toDispose = tabBarItem.render()
    const element = tabBarItem.getElement()
    if (element) {
      this._tabContainer.appendChild(element)
    }
    this._itemList.push(tabBarItem)
    return {
      dispose: () => {
        if (toDispose) {
          toDispose.dispose()
        }
        tabBarItem.destroy()
      },
    }
  }

  dispose(): void {
    this._toDispose = dispose(this._toDispose)
    super.dispose()
  }
}
