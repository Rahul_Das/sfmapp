'use strict'

import { ITabBarItem } from './tabbarItem'

export const HomeItems: ITabBarItem = {
  id: 'tb0',
  icon: {
    name: 'notification',
    className: 'statusber-icon',
    height: '22',
    weidth: '22',
  },
  tooltip: 'No Notification',
  color: 'white',
}
