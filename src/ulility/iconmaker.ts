//import {classids} from '../layoutConstant'

import {IIconmakerOptions} from './iconMakerOpt'

export interface IIcondata {
  name: string
  viewBox: string
  paths: string[]
}

const iconList: IIcondata[] = [
  {
    name: 'account',
    viewBox: '0 0 24 24',
    paths: ['M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z'],
  },
  {
    name: 'addProject',
    viewBox: '0 0 24 24',
    paths: ['M9 13h6m-3-3v6m-9 1V7a2 2 0 012-2h6l2 2h6a2 2 0 012 2v8a2 2 0 01-2 2H5a2 2 0 01-2-2z'],
  },
  {
    name: 'settings',
    viewBox: '0 0 24 24',
    paths: [
      'M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z',
      'M15 12a3 3 0 11-6 0 3 3 0 016 0z',
    ],
  },
  {
    name: 'notification',
    viewBox: '0 0 24 24',
    paths: [
      'M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9',
    ],
  },
]

export const creteIcons: (options: IIconmakerOptions) => SVGElement = (options) => {
  const NAMESPACE = 'http://www.w3.org/2000/svg'
  const item: IIcondata = iconList.filter((data: IIcondata) => data.name === options.name)[0]

  const iconSvg = document.createElementNS(NAMESPACE, 'svg')

  iconSvg.setAttribute('fill', 'none')
  iconSvg.classList.add(options.className)
  iconSvg.setAttribute('viewBox', item.viewBox)
  iconSvg.setAttribute('stroke', options.color || 'white')
  iconSvg.setAttribute('height', options.height || '32')
  iconSvg.setAttribute('weidth', options.weidth || '32')

  item.paths.forEach((path) => {
    const iconPath = document.createElementNS(NAMESPACE, 'path')
    iconPath.setAttribute('d', path)
    iconPath.setAttribute('stroke-linecap', 'round')
    iconPath.setAttribute('stroke-linejoin', 'round')
    iconPath.setAttribute('stroke-width', '1')
    iconSvg.appendChild(iconPath)
  })

  return iconSvg
}
