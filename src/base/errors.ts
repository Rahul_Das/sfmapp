'use strict'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TError = unknown

export interface IErrorListenerCallback {
  (error: TError): void
}

export interface IErrorListenerUnbind {
  (): void
}

class ErrorHandler {
  private unexpectedErrorHandler: (e: TError) => void
  private listeners: IErrorListenerCallback[]

  constructor() {
    this.listeners = []

    this.unexpectedErrorHandler = function (e: TError) {
      setTimeout(() => {
        if (e instanceof Error && e.stack) {
          throw new Error(e.message + '\n\n' + e.stack)
        }
        throw e
      }, 0)
    }
  }

  addListener(listener: IErrorListenerCallback): IErrorListenerUnbind {
    this.listeners.push(listener)
    return () => {
      this._removeListener(listener)
    }
  }

  private emit(e: TError): void {
    this.listeners.forEach((listener) => {
      listener(e)
    })
  }

  private _removeListener(listener: IErrorListenerCallback): void {
    this.listeners.splice(this.listeners.indexOf(listener), 1)
  }
  setUnexpectedErrorHandler(newUnexpectedErrorHandler: (e: TError) => void): void {
    this.unexpectedErrorHandler = newUnexpectedErrorHandler
  }
  getUnexpectedErrorHandler(): (e: TError) => void {
    return this.unexpectedErrorHandler
  }
  onUnexpectedError(e: TError): void {
    this.unexpectedErrorHandler(e)
    this.emit(e)
  }
  // For external errors, we don't want the listeners to be called
  onUnexpectedExternalError(e: TError): void {
    this.unexpectedErrorHandler(e)
  }
}

export const errorHandler = new ErrorHandler()

export function onUnexpectedError(e: TError): void {
  errorHandler.onUnexpectedError(e)
}
