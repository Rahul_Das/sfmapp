'use strict'

import {IDisposable} from '../disposable'

export interface IListenerCallback {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (value: any): void
}

export interface IListenersMap {
  [key: string]: Function[]
}

export interface IEventData {
  [key: string]: string | number | string[] | number[]
}

export interface IEventEmitter extends IDisposable {
  addListener(eventType: string, listener: Function): IDisposable
  addOneTimeDisposableListener(eventType: string, listener: Function): IDisposable

  emit(eventType: string, data: Object): void
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function safeInvokeArg(func: Function, arg: Object): any {
  try {
    return func(arg)
  } catch (error: unknown) {
    if (error instanceof Error && error.stack) {
      throw new Error(error.message + '\n\n' + error.stack)
    }
    throw error
  }
}

export class EventEmitter implements IEventEmitter {
  protected _listeners: IListenersMap
  private _deferredCnt: number
  private _allowedEventTypes: {[eventType: string]: boolean} | null

  constructor(allowedEventTypes: string[] | null = null) {
    this._listeners = {}
    this._deferredCnt = 0
    if (allowedEventTypes) {
      this._allowedEventTypes = {}
      for (let i = 0; i < allowedEventTypes.length; i++) {
        this._allowedEventTypes[allowedEventTypes[i]] = true
      }
    } else {
      this._allowedEventTypes = null
    }
  }

  addListener(eventType: string, listener: Function): IDisposable {
    if (this._allowedEventTypes && !this._allowedEventTypes.hasOwnProperty(eventType)) {
      throw new Error('This object will never emit this event type!')
    }

    if (this._listeners.hasOwnProperty(eventType)) {
      this._listeners[eventType].push(listener)
    } else {
      this._listeners[eventType] = [listener]
    }
    let bound = this
    return {
      dispose: () => {
        if (!bound) {
          return
        }
        bound._removeListener(eventType, listener)
        bound = {} as typeof this
        listener = {} as Function
      },
    }
  }

  addOneTimeDisposableListener(eventType: string, listener: Function): IDisposable {
    const disposable = this.addListener(eventType, (value: Object) => {
      disposable.dispose()
      listener(value)
    })
    return disposable
  }

  private _removeListener(eventType: string, listener: Function): void {
    if (this._listeners.hasOwnProperty(eventType)) {
      const listeners = this._listeners[eventType]
      for (let i = 0, len = listeners.length; i < len; i++) {
        if (listeners[i] === listener) {
          listeners.splice(i, 1)
          break
        }
      }
    }
  }

  emit(eventType: string, data: Object = {}): void {
    if (this._allowedEventTypes && !this._allowedEventTypes.hasOwnProperty(eventType)) {
      throw new Error('Cannot emit this event type because it was not white-listed!')
    }
    if (!this._listeners.hasOwnProperty(eventType) && this._listeners[eventType].length === 0) {
      return
    }
    this._emitToSpecificTypeListeners(eventType, data)
  }

  protected _emitToSpecificTypeListeners(eventType: string, data: Object): void {
    if (this._listeners.hasOwnProperty(eventType)) {
      for (let i = 0, len = this._listeners[eventType].length; i < len; i++) {
        safeInvokeArg(this._listeners[eventType][i], data)
      }
    }
  }

  dispose(): void {
    this._listeners = {}
    this._deferredCnt = 0
    this._allowedEventTypes = null
  }
}
