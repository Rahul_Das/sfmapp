'use strict'

import {Statusbar} from './components/statusbar/statusbar'
import {Activitybar} from './components/activitybar/activitybar'
import {Sidebar} from './components/sidebar/sidebar'
import {Playground} from './components/mainsection/mainsection'
import {Box, Dimension} from './components/interfaces'

export interface IAppLayoutInfo {
  titlebar: {height: number}
  activitybar: {width: number}
  sidebar: {minWidth: number}
  panel: {minHeight: number}
  editor: {minWidth: number; minHeight: number}
  statusbar: {height: number}
}

export class LayoutOptions {
  margin: Box

  constructor() {
    this.margin = new Box(0, 0, 0, 0)
  }

  setMargin(margin: Box): LayoutOptions {
    this.margin = margin
    return this
  }
}

interface ComputedStyles {
  activitybar: {minWidth: number}
  sidebar: {minWidth: number}
  editor: {minWidth: number}
  statusbar: {height: number}
}

export class AppLayout {
  private toUnbind: {(): void}[]
  private computedStyles: ComputedStyles | null

  constructor(
    private parent: HTMLElement,
    private workbenchContainer: HTMLElement,
    private activitybar: Activitybar,
    private playground: Playground,
    private sidebar: Sidebar,
    private statusbar: Statusbar,
    private options: LayoutOptions,
    private editorHeight: number,
    private workbenchSize: Dimension,
    private startSidebarWidth: number,
    private sidebarWidth: number
  ) {
    this.options = options || new LayoutOptions()
    this.toUnbind = []
    this.computedStyles = null
    //this.sidebarWidth = this.storageService.getInteger(WorkbenchLayout.sashWidthSettingsKey, StorageScope.GLOBAL, -1)
    this.registerListeners()
    this.registerSashListeners()
  }

  registerListeners() {}

  registerSashListeners() {}

  private computeStyle(): void {
    const sidebarStyle = this.sidebar.getComputedStyle()
    const playgroundStyle = this.playground.getComputedStyle()
    const activitybarStyle = this.activitybar.getComputedStyle()

    this.computedStyles = {
      activitybar: {
        minWidth: parseInt(activitybarStyle.getPropertyValue('min-width'), 10) || 0,
      },

      sidebar: {
        minWidth: parseInt(sidebarStyle.getPropertyValue('min-width'), 10) || DEFAULT_MIN_PART_WIDTH,
      },

      editor: {
        minWidth: parseInt(playgroundStyle.getPropertyValue('min-width'), 10) || DEFAULT_MIN_PART_WIDTH,
      },

      statusbar: {
        height: 0,
      },
    }

    if (this.statusbar) {
      const statusbarStyle = this.statusbar.getComputedStyle()
      this.computedStyles.statusbar.height = parseInt(statusbarStyle.getPropertyValue('height'), 10) || 18
    }
  }

  layout(forceStyleReCompute?: boolean): void {
    if (forceStyleReCompute) {
      this.computeStyle()
      this.playground.getLayout().computeStyle()
      this.sidebar.getLayout().computeStyle()
    }
    if (!this.computedStyles) {
      this.computeStyle()
    }
  }
}
