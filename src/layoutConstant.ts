'use strict'

export const identifier = {
  APP_ID: 'app-container-id',
  statusbar: {
    id: 'app-statusbar',
  },
  sidebar: {
    id: 'app-sidebar',
  },
  mainsection: {
    id: 'app-mainsection',
  },
  tabbar: {
    id: 'app-tabbar',
  },
}

export const classids = {
  APP_CLASS: 'app-container',
  statusbar: {
    parent: 'statusbar',
    content: 'statusbar-content',
    item: 'statusbar-item',
  },
  sidebar: {
    parent: 'sidebar',
    content: 'sidebar-content',
    item: 'sidebar-item',
  },
  tabbar: {
    parent: 'tabbar',
    content: 'tabbar-content',
    item: 'tabbar-item',
  },
  mainsection: {
    parent: 'mainsection',
    content: 'mainsection-content',
    item: 'mainsection-item',
  },
}
