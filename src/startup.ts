'use strict'

import './style.scss'

import { identifier, classids } from './layoutConstant'
import { IStatusBar, StatusBar } from './components/statusbar/statusbar'
import { ISideBar, SideBar } from './components/sidebar/sidebar'
import { ITabBar, TabBar } from './components/tabbar/tabbar'
import { MainSection } from './components/mainsection/mainsection'


import { invoke } from '@tauri-apps/api/tauri'
import { appWindow, PhysicalPosition, PhysicalSize } from '@tauri-apps/api/window'
import { Component } from './components/components'


export class StartUp {
  private container: HTMLDivElement
  private statusBar!: IStatusBar
  private sideBar!: ISideBar
  private tabBar!: ITabBar
  private mainsection!: Component

  constructor() {
    this.container = document.getElementById(identifier.APP_ID) as HTMLDivElement
    this.container.classList.add(classids.APP_CLASS)
  }

  startup() {
    try {
      this.registerListeners()
      this.initSettings()
      this.renderComponents()

    } catch (error) {
      console.error(error)
      throw error
    }
  }

  renderComponents() {
    this.renderStatusBar()
    this.renderSideBar()
    this.renderTabBar()
    this.renderMainSection()
  }

  private renderMainSection() {
    const mainsSectionParent = document.createElement('div') as HTMLDivElement
    mainsSectionParent.classList.add(classids.mainsection.parent)
    mainsSectionParent.setAttribute('id', identifier.mainsection.id)
    this.container.appendChild(mainsSectionParent)
    this.mainsection = new MainSection(identifier.mainsection.id)
    this.mainsection.create(mainsSectionParent)
  }

  private renderStatusBar() {
    const statusBarParent = document.createElement('div') as HTMLDivElement
    statusBarParent.classList.add(classids.statusbar.parent)
    statusBarParent.setAttribute('id', identifier.statusbar.id)
    this.container.appendChild(statusBarParent)
    this.statusBar = new StatusBar(identifier.statusbar.id)
    this.statusBar.create(statusBarParent)
  }

  private renderSideBar() {
    const sideBarParent = document.createElement('div') as HTMLDivElement
    sideBarParent.classList.add(classids.sidebar.parent)
    sideBarParent.setAttribute('id', identifier.sidebar.id)
    this.container.appendChild(sideBarParent)
    this.sideBar = new SideBar(identifier.sidebar.id)
    this.sideBar.create(sideBarParent)
  }

  private renderTabBar() {
    const tabbarParent = document.createElement('div') as HTMLDivElement
    tabbarParent.classList.add(classids.tabbar.parent)
    tabbarParent.setAttribute('id', identifier.tabbar.id)
    this.container.appendChild(tabbarParent)
    this.mainsection = new TabBar(identifier.tabbar.id)
    this.mainsection.create(tabbarParent)
  }

  private registerListeners() {
    appWindow.listen('config-init', event => {
      console.log('registerListeners :', event.payload, event.event)
    })
    appWindow.listen('page-loaded', event => {
      //console.log('page-loaded :', event.payload, event.event)
      appWindow.show()
    })
    appWindow.once('tauri://close-requested', this.window_close_handeler)
    appWindow.once('tauri://destroyed', this.window_close_handeler)
    appWindow.listen('tauri://resize', ({ event, payload }) => {
      console.log('resize event trigered :', payload, event)
      const { width, height } = payload as PhysicalSize
      invoke('plugin:config|change_dimension', { height: height, width: width })
    })
    appWindow.listen('tauri://move', ({ event, payload }) => {
      const { x, y } = payload as PhysicalPosition
      console.log('Move event trigered :', x, y)
    })
  }

  private window_close_handeler() {
    console.log('window Close triggered')
    invoke('plugin:config|save_config')
      .then(() => console.log('config saved'))
      .catch((error) => console.error(error))
  }

  initSettings() {
    invoke('plugin:config|get_config')
      .then((config) => console.log('config ', config))
      .catch((error) => console.error(error))
  }
}
